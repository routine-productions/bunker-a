<?php
use Illuminate\Database\Eloquent\Model as Model;
class Comments extends Model {
    public $timestamps = false;
    public $table = 'comments';
}

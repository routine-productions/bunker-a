<div class="Module-Map Shadow">
    <div class="Info">
        <h2>Как добраться?</h2>
        <p class="Italic">“Бункер - А” находится в центре города в пятиэтажке, по адресу ул. Апатова 147, в дальнем торце здания.</p>
        <p>Ориентиры:<br>
            - пересечение ул. Апатова и ул. Осипенко<br>
            - магазин “Экватор”<br>
            - школа №3<br>
        </p>

        <p>Как пройти от остановки:<br>
            - "ул. Леваневского" <span style="  color: rgb(219, 68, 54);">[ красный ]</span><br>
            - "Тысяча мелочей" или "ТЦ Обжора" <span style="color: rgb(63, 91, 169);">[ синий ]</span><br>
            - “Дом связи” <span style="color: rgb(143, 70, 166);">[ фиолетовый ]</span><br>
            - “Подземный переход” <span style="  color: rgb(34, 130, 87);">[ зеленый ]</span><br>
            - “Кинотеатр Комсомолец” <span style="color: rgb(244, 180, 0);">[ желтый ]</span>
        </p>
    </div>

    <iframe class="Map-Canvas" src="https://mapsengine.google.com/map/embed?mid=zJaDbMIi5bXs.kxdkjx6clURE"></iframe>
</div>
<div class="Module-Events">

    <div class="Event">
        <a href='/events/mariupol-covered-the-ocean.html' class="Previous Light"></a>
        <div class="Info Right">
            <h2>Мариуполь перепел Океан</h2>

            <p>Кавер-версия Океан Ельзи - "Обійми".</p>
            <p>Мариупольские рок-музыканты отблагодарили Святослава Вакарчука за концерт в г. Мариуполе видеоклипом с неожиданным изложением лирического хита "Обiйми".</p>

            <i>с 24 сентября 2016</i>
        </div>
        <div class="Picture" style="background:url('/pictures/events/2016.09.24_mariupol-covered-the-ocean.jpg') center top;"></div>
    </div>


    <div class="Event">
        <a href='/events/bunker_live' class="Previous Light"></a>
        <div class="Info Right">
            <h2>Музыкальный проект “Bunker Live”</h2>

            <p>Живой звук в студии "Бункер-А" от лучших музыкальных коллективов Приазовья на канале <a href="http://www.mtv.pics/">МТВ</a>.</p>
            <p>Группа победившая в СМС голосовании на открытии "Бункер - А" будет приглашена на первый эфир музыкальной проекта "Bunker Live".</p>
            <a href='/events/bunker_live' class="More">Подробнее</a>
            <i>с 27 июня 2015</i>
        </div>
        <div class="Picture" style="background:url('/pictures/events/07.06.2015_bunker_live.jpg');"></div>
    </div>

    <div class="Event">
        <a href="/events/opening" class="Previous Light"></a>
        <div class="Info Right">
            <h2>Открытие музыкального клуба "Бункер - А"</h2>
            <p>Презентация клуба, концерт мариупольских коллективов, эфир на <a href="http://www.mtv.pics/">МТВ</a>.</p>
            <p>На открытии Вы узнаете про историю создания и формат "Бункера", а также послушаете и познакомитесь с лучшими группами г. Мариуполя.</p>
            <p>Лучшая группа по итогам СМС голосования будет приглашена на первый эфир музыкального проекта "Bunker Live", а так же три лидера СМС голосования получат пакет бесплатных репетиций.</p>
            <a href='/events/opening' class="More">Подробнее</a>
            <i>31 мая 2015, 15:00</i>
        </div>
        <div class="Picture" style="background:url('/pictures/events/16.05.2015_opening.jpg');"></div>
    </div>


    <div class="Event">
        <a href="/events/first_repetition" class="Previous Light"></a>
        <div class="Info">
            <h2>Пробная репетиция</h2>

            <p>Музыкальный клуб "Бункер - А" приглашает музыкальные коллективы на 3-х часовую пробную репетицию, во
                время которой Вы сможете ознакомиться с оборудованием, узнать о работе и проектах клуба. Пробные
                репетиции проводятся до открытия с 12 по 24 мая.</p>

            <p>После открытия смотрите расписание и бронируйте время на сайте. Сейчас заявки на репетиции принимаются по
                номеру +38 (098) 444-30-62 / Виталий.</p>
            <a href='/events/first_repetition' class="More">Подробнее</a>
            <i>с 12 мая 2015</i>
        </div>
        <div class="Picture" style="background:url('/pictures/events/25.04.2015_free_repetition.jpg');"></div>
    </div>
</div>
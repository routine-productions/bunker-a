<div class="Module-About Shadow">
    <div class="Repetition">
        <div class="Info">
            <h2>Репетиционная база</h2>
            <p>
                - комфортные условия репетиций;<br>
                - профессиональный аппарат;<br>
                - студийный звук;<br>
                - круглосуточный график;<br>
                - удобное месторасположение;<br>
                - творческое настроение.<br>
            </p>
            <a class="More" href="/repetition">Подробнее о репетициях</a>
            <a href="/repetition" class="Next"></a>
        </div>
        <div class="Photo"  style="background:url('/pictures/about/repetition.jpg');"></div>
    </div>
    <div class="Studio">
        <div class="Photo" style="background:url('/pictures/about/studio.jpg');"></div>
        <div class="Info">
            <h2>Студия звукозаписи</h2>
            <p>
                - все необходимое оборудование;<br>
                - многоканальная потрековая запись;<br>
                - скорость работы;<br>
                - рабочая атмосфера;<br>
                - продвижение материала на ресурсах "Бункера".<br>
            </p>
            <a class="More" href="/studio">Подробнее о записи</a>
            <a href="/studio" class="Next"></a>
        </div>
    </div>
    <div class="Club">
        <div class="Info">
            <h2>Музыкальный клуб</h2>
            <p>
                - музыкальный проект "Bunker Live";<br>
                - регулярные батлы между группами;<br>
                - мастер классы;<br>
                - тематические тусовки;<br>
                - онлайн-радио;<br>
                - музыкальные передачи.<br>
            </p>
            <a class="More" href="/about">Подробнее о клубе</a>
            <a href="/about" class="Next"></a>
        </div>
        <div class="Photo" style="background:url('/pictures/about/club.jpg');"></div>
    </div>
</div>
<?php
require './app/models/comments.php';
$Comments = Comments::where('alias', $URI[0])->orderBy('created_at')->get()->toArray()  ;
?>

<div class="Comments">
    <h2>Комментарии</h2>
    <div class="Form">
        <textarea placeholder="Что скажешь?"></textarea>
        <button>Отправить</button>
    </div>
    <div class="List">
        <?php foreach ($Comments as $Key => $Comment) { ?>

            <div class="Comment">
                <div class="Login"><?= $Comment['login'] ?></div>
                <div class="Text"><?= $Comment['comment'] ?></div>
                <div class="Rate"><?= $Comment['rate'] ?></div>
                <div class="Rate"><?= $Comment['updated_at'] ?></div>
            </div>

        <?php } ?>
    </div>
</div>
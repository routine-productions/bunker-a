<?php

if ($URI[0]) {
    $File = './app/sections/' . $URI[0] . '/index.php';
    if (file_exists($File)) {
        require_once $File;
    } else {
        require_once './app/sections/loading/index.php';
    }

} else {
    require_once './app/sections/_home/index.php';
}
<?php
if (!isset($URI[1])) {
    ?>
    <h1 class="Header">События</h1>
    <?php
    require "./app/modules/events/index.php";
} elseif ($URI[1] == 'first_repetition') {
    require "first_repetition.php";
} elseif ($URI[1] == 'opening') {
    require "opening.php";
} elseif ($URI[1] == 'bunker_live') {
    require "bunker_live.php";
}
?>
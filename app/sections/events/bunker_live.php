<div class="Content Fixed">
    <div class="Text">
        <h1>Музыкальный проект “Bunker Live”</h1>

        <p>Живой звук в студии "Бункер-А" от лучших музыкальных коллективов Приазовья на канале <a href="http://www.mtv.pics/">МТВ</a>.</p>
        <p>Группа победившая в СМС голосовании на открытии "Бункер - А" будет приглашена на первый эфир музыкальной проекта "Bunker Live".</p>
        <p>Информация обновляется.</p>
        <i>с 27 июня 2015, 17:00</i>

    </div>
    <div class="Photo" style="background:url('/pictures/events/07.06.2015_bunker_live.jpg');"></div>
</div>
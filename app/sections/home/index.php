<div class="Menu Shadow">
    <a href="/enter" data-ajax="/module/enter" class="User"><i class="fa fa-user"></i></a>

    <div class="Left-Side">
        <a href="/about" title="О клубе">О клубе</a>
        <a href="/record" title="Запись">Запись</a>
        <a href="/repetition" title="Репетиции">Репетиции</a>
        <a href="/timetable" title="Расписание">Расписание</a>
    </div>
    <a href="/" class="Logo"><img src='/img/logo/small-logo.png' alt="Бункер - А"></a>

    <div class="Right-Side">
        <a href="/timetable" title="События">События</a>
        <a href="/articles" title="Статьи">Статьи</a>
        <a href="/creative" title="Творчество">Творчество</a>
        <a href="/discussion" title="Общение">Общение</a>
    </div>

    <a href="/search" data-ajax="/module/search" class="Search"><i class="fa fa-search"></i></a>
</div>

<?php require $_SERVER['DOCUMENT_ROOT'] . "/app/modules/panorama/index.php"; ?>
<?php require $_SERVER['DOCUMENT_ROOT'] . "/app/modules/about/index.php"; ?>
<?php require $_SERVER['DOCUMENT_ROOT'] . "/app/modules/events/index.php"; ?>
<?php require $_SERVER['DOCUMENT_ROOT'] . "/app/modules/gallery/index.php"; ?>
<?php require $_SERVER['DOCUMENT_ROOT'] . "/app/modules/map/index.php"; ?>


<div class="Footer Shadow">
    <div class="Wrapper">
        <div class="Wrapper-Box">
            <p class="About">
                «Бункер - А»<br>
                Музыкальный клуб<br>
                Репетиционная база<br>
                Студия звукозаписи
            </p>

            <div class="Logo">
                <img class="Round-Logo" src="/img/logo/logo-loading.png" alt="Бункер - А">

                <div class="Round"></div>
                <div class="Round-Inner"></div>
                <div class="Round-Outer"></div>
            </div>

            <p class="Contacts">
                +38 (098) 444-30-62 / Виталий<br>
                inbox@bunker-a.com<br>
                http://bunker-a.com<br>
                Мариуполь, ул. Апатова 147, Bunker - A
            </p>
        </div>
    </div>
</div>
$(document).ready(function () {

    /*** Preloading ***/

    $(window).load(function () {
        $('.Pre-Loading').animate({'top': - (1.5 * $(window).height()) + 'px'}, 300, function(){
            $('.Pre-Loading').remove();
        });


        /*** Nicescroll ***/
        $("html").niceScroll({
            cursorborder: 'none',
            cursorborderleft: '0px solid #a8a8a8',
            cursorwidth: "12px",
            cursorborderradius: '0',
            cursorcolor: '#a8a8a8',
            zindex: '100000',
            cursoropacitymin:'0.8'
        });

        /*** Nicescroll ***/
        $(".Content.Fixed .Text").niceScroll({
            cursorborder: 'none',
            cursorwidth: "10px",
            cursorborderradius: '0',
            cursorcolor: '#242424',
            zindex: '100000',
            cursoropacitymin:'0.3'
        });



    });

    /*** Preloading ***/





    /*** Map Prevent ***/

    $('.Map-Canvas').addClass('Scroll-Off');

    $('.Map').on('click', function () {
        $('.Map-Canvas').removeClass('Scroll-Off');
    });

    $(".Map-Canvas").mouseleave(function () {
        $('.Map-Canvas').addClass('Scroll-Off');
    });

    /*** Map Prevent ***/





    /*** Slider-Module ***/

    var ShowWave = '';
    $('.Module-Slider .Previous').click(function () {
        $('.Module-Slider .Wave').css('opacity', '0');
        clearTimeout(ShowWave);
        ShowWave = setTimeout(function () {
            $('.Module-Slider .Wave').css('opacity', '0.64');
        }, 5000);
        $('.Module-Slider').animate({'background-position-x': (parseInt($('.Module-Slider').css('background-position-x')) + 400) + 'px'}, 400,'linear');
    });

    $('.Module-Slider .Next').click(function () {
        $('.Module-Slider .Wave').css('opacity', '0');
        clearTimeout(ShowWave);
        ShowWave = setTimeout(function () {
            $('.Module-Slider .Wave').css('opacity', '0.64');
        }, 5000);
        $('.Module-Slider').animate({'background-position-x': (parseInt($('.Module-Slider').css('background-position-x')) - 400) + 'px'}, 400,'linear');
    });

    /*** Slider-Module ***/





    /*** Slider ***/

    $('.Module-Gallery').lbSlider({
        leftBtn: '.Module-Gallery .Previous',
        rightBtn: '.Module-Gallery .Next',
        visible: 2,
        autoPlay: true,
        autoPlayDelay: 10
    });

    /*** Slider ***/

    $('.Menu .User,.Menu .Search').upf_window();


    /*** Footer ***/
    function PressFooter(){
        $('.Footer').css({'position':'relative'});

        if(($('.Footer').position().top + $('.Footer').height()) < $(window).height()){
            $('.Footer').css({'position':'fixed','bottom':'0'});
        }else{
            $('.Footer').css({'position':'relative'});
        }
    }

    PressFooter();
    $(window).resize(function(){
        PressFooter();
    });

    /*** Search ***/
    $(window).on('click','.Module-Search button',function(){
        location.href = '/search?phrase=' + $('.Module-Search input[name=phrase]').val();
        return false;
    });

    /*** Enter ***/
    $(window).on('click','.Module-Enter button',function(){
        location.href = '/registration';
        return false;
    });
});



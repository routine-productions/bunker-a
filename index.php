<?php
error_reporting(E_ALL);
require_once './app/index.php';
?>
<!DOCTYPE html>
<html class="HTML-<?= isset($URI[0]) ? ucfirst($URI[0]) : 'HTML-Home' ?>">
<head>
    <title>Бункер - А</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="vyWXuuiDs_mt0uDIlCM3N2w2FJjYXqBfqylFPg77lfs" />

    <meta name="description" content="'Бункер - А': Репетиционная база - Студия звукозаписи - Музыкальный клуб">
    <meta name="keywords"
          content="Студия, звукозапись, репетиционная, музыкальный клуб, Мариуполь, Украина, Донецкая область">
    <link rel="icon" type="image/png" href="/img/fav.png">
    <link rel="alternate" hreflang="ru" href="http://bunker-a.com/"/>

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,700,300,600,400&subset=latin,cyrillic-ext'
          rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/scss/compressed.css"/>

    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/scss/ie8.css"/>
    <![endif]-->
</head>

<body class="<?= isset($URI[0]) ? ucfirst($URI[0]) : 'Home' ?>">

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-41244531-2', 'auto');
    ga('send', 'pageview');
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter30163389 = new Ya.Metrika({id:30163389,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/30163389" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<div class="Pre-Loading">
    <img class="Round-Logo" src="/img/logo/logo-loading.png" alt="Бункер - А">

    <div class="Round"></div>
    <div class="Round-Inner"></div>
    <div class="Round-Outer"></div>

    <p class="About">Бункер - А<br>Музыкальный клуб<br>Студия звукозаписи<br>Репетицонная база</p>

    <p class="Info">+38 (098) 444-30-62 / Виталий<br>inbox@bunker-a.com<br>http://bunker-a.com<br>Мариуполь, ул. Апатова
        147, Bunker - A</p>
</div>

<?php if($ShowMenu){ ?>
<div class="Menu Shadow">
    <a href="/enter" data-ajax="/module/enter" class="User"><i class="fa fa-user"></i></a>

    <div class="Left-Side">
        <a href="/about" title="О клубе">О клубе</a>
        <a href="/record" title="Запись">Запись</a>
        <a href="/repetition" title="Репетиции">Репетиции</a>
        <a href="/timetable" title="Расписание">Расписание</a>
    </div>
    <a href="/" class="Logo"><img src='/img/logo/small-logo.png' alt="Бункер - А"></a>

    <div class="Right-Side">
        <a href="/events" title="События">События</a>
        <a href="/articles" title="Статьи">Статьи</a>
        <a href="/creative" title="Творчество">Творчество</a>
        <a href="/discussion" title="Общение">Общение</a>
    </div>

    <a href="/search" data-ajax="/module/search" class="Search"><i class="fa fa-search"></i></a>
</div>
<?php } ?>


<?php require_once './app/router.php'; ?>

<?php if($ShowMenu){ ?>
<div class="Footer Shadow">
    <div class="Wrapper">
        <div class="Wrapper-Box">
            <p class="About">
                «Бункер - А»<br>
                Музыкальный клуб<br>
                Репетиционная база<br>
                Студия звукозаписи
            </p>

            <div class="Logo">
                <img class="Round-Logo" src="/img/logo/logo-loading.png" alt="Бункер - А">
                <div class="Round"></div>
                <div class="Round-Inner"></div>
                <div class="Round-Outer"></div>
            </div>

            <p class="Contacts">
                +38 (098) 444-30-62 / Виталий<br>
                inbox@bunker-a.com<br>
                <a href="http://vk.com/bunker_studio">vk.com/bunker_studio</a><br>
                Мариуполь, ул. Апатова 147, Bunker - A
            </p>
        </div>
    </div>
</div>
<?php } ?>



<script src="/js/compressed.js"></script>


</body>
</html>